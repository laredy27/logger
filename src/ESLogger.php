<?php


namespace laredy\logger;


use Psr\Log\AbstractLogger;
use Psr\Log\InvalidArgumentException;
use \Exception;

class ESLogger extends AbstractLogger
{

    /**
     * @var ESPublisher
     */
    private $publisher;

    public function __construct(ESPublisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public function doLog($level, $message)
    {
        if ( is_object($message) ) {
            $message = json_encode($message);
        }
        $this->publisher->publish($message);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        if (!method_exists($this, $level)) {
            throw new InvalidArgumentException("Unknown log level $level");
        }

        $JSONMessage = new \stdClass();

        // after context merging might have been done

        if (array_key_exists('exception', $context) && is_a($context['exception'], \Exception::class)) {
            $ex = $context['exception'];
            $JSONMessage->exception = true;
            $JSONMessage->stackTrace = $ex->getTraceAsString();
            $JSONMessage->exceptionMessage = $ex->getMessage();
        }


        $JSONMessage->message =  method_exists($message, '__toString') ? $message->__toString() : (string) $message;


        $this->doLog($level, $JSONMessage);
    }

    public function getPublisher()
    {
        return $this->publisher;
    }
}
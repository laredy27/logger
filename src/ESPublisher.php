<?php


namespace laredy\logger;


class ESPublisher
{
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $index;

    public function __construct(string $host, string $index = "logs")
    {
        $this->host = $host;
        $this->index = $index;
    }

    public function publish($message)
    {
        $message = json_decode($message);
        $message->timestamp = (new \DateTime())->format('c');
        $message->date_added = date('Y-m-d H:i:s');
        $message->host = gethostname();
        $message->ipaddress = $_SERVER['REMOTE_ADDR'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'];

        $message = json_encode($message);
        return $this->send($message);
    }

    private function send(string $message) : bool
    {
        $ch = curl_init($this->host);
        curl_setopt($ch, CURLOPT_URL, $this->host . '/' . $this->index . '/_doc');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: application/json',
            'Content-Type: application/json'
        ]);

        curl_exec($ch);

        if (curl_error($ch)) {
            error_log("Failed to publish record '$message' to elastic search:");
            curl_close($ch);
            return false;
        } else {
            curl_close($ch);
            return true;
        }
    }
}
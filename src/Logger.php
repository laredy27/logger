<?php

namespace laredy\logger;

use Psr\Log\AbstractLogger;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;

class Logger
{

    /** @var LoggerInterface */
    static private $instance;

    static public function setInstance(LoggerInterface $logger)
    {
        self::$instance = $logger;
    }

    static public function getInstance()
    {
        return self::$instance;
    }

    static public function deferToInstance($method, $message, array $context)
    {
        try {
            self::$instance->$method($message, $context);
        } catch(InvalidArgumentException $exception) {
            return false;
        }
    }

    static public function debug($message, array $context = [])
    {
        self::deferToInstance('debug', $message, $context);
    }

    static public function info($message, array $context = [])
    {
        self::deferToInstance('info', $message, $context);
    }

    static public function notice($message, array $context = [])
    {
        self::deferToInstance('notice', $message, $context);
    }

    static public function warning($message, array $context = [])
    {
        self::deferToInstance('warning', $message, $context);
    }

    static public function error($message, array $context = [])
    {
        self::deferToInstance('error', $message, $context);
    }

    static public function critical($message, array $context = [])
    {
        self::deferToInstance('critical', $message, $context);
    }

    static public function alert($message, array $context = [])
    {
        self::deferToInstance('alert', $message, $context);
    }

    static public function emergency($message, array $context = [])
    {
        self::deferToInstance('emergency', $message, $context);
    }


}
<?php

namespace laredy\logger\test;

use laredy\logger\ESLogger;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use laredy\logger\ESPublisher;

class ESLoggerTest extends TestCase
{

    use MockeryPHPUnitIntegration;
    /**
     * @var ESLogger
     */
    private $sut;

    protected function setUp()
    {
        parent::setUp();

//        $pub = new ESPublisher('https://search-baroninc-vbebju5o6rbiqkxkosihk6mqj4.eu-west-2.es.amazonaws.com');
        $pub = \Mockery::spy(ESPublisher::class);
        $this->sut = new ESLogger($pub);
    }

    public function testLogSimpleMessage()
    {
        $message = "i quite like elastic search!";
        $context = [];

        $this->sut->log('debug', $message, $context);
        $this->sut->getPublisher()->shouldHaveReceived('publish')->once();
    }

    public function testLogComplexMessage()
    {
        $message =  new class('this is a complex message') {

            public $message;

            public function __construct($message)
            {
                $this->message = $message;
            }

            public function __toString() : string
            {
                return $this->message;
            }
        };
        $context = [];

        $this->sut->log('debug', $message, $context);
        $this->sut->getPublisher()->shouldHaveReceived('publish')->once();
    }

    public function testLogException()
    {
        $ex = \Mockery::spy(\InvalidArgumentException::class, ['This is a test exception message'])->makePartial();
        $context['exception'] = $ex;
        $this->sut->log('debug', 'i am a log message2', $context);
        $this->sut->getPublisher()->shouldHaveReceived('publish')->once();
    }

    public function testDoLog()
    {
        $this->assertTrue(true);
    }
}
